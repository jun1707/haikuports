SUMMARY="QR Code encoding library"
DESCRIPTION="
QLibqrencode is a library for encoding data in a QR Code symbol, a kind of 2D \
symbology that can be scanned by handy terminals such as a mobile phone with \
CCD. The capacity of QR Code is up to 7000 digits or 4000 characters, and has \
high robustness.

Libqrencode accepts a string or a list of data chunks then encodes in a QR Code \
symbol as a bitmap array. While other QR Code applications generate an image \
file, using libqrencode allows applications to render QR Code symbols from raw \
bitmap data directly. This library also contains a command-line utility outputs \
a QR Code symbol as a PNG image. It will help light-weight CGI programs.
"
HOMEPAGE="http://fukuchi.org/works/qrencode/"
COPYRIGHT="
	Copyright (C) 2006-2012 Kentaro Fukuchi
	"
LICENSE="GNU LGPL v2.1"
SRC_URI="http://fukuchi.org/works/qrencode/qrencode-$portVersion.tar.gz"
CHECKSUM_SHA256="499b2c86c218b16378e2496fefc858e4db4271c048336a29df9cc5754413d1d1"
REVISION="3"
ARCHITECTURES="x86_gcc2 x86 x86_64"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"

PROVIDES="
	qrencode$secondaryArchSuffix = $portVersion
	lib:libqrencode$secondaryArchSuffix  = 3.3.0 compat >= 3
	"
REQUIRES="
	haiku$secondaryArchSuffix
	lib:libpng16$secondaryArchSuffix
	lib:libz$secondaryArchSuffix
	"
BUILD_REQUIRES="
	devel:libpng16$secondaryArchSuffix
	devel:libz$secondaryArchSuffix
	"
BUILD_PREREQUIRES="
	haiku${secondaryArchSuffix}_devel
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:make
	cmd:pkg_config$secondaryArchSuffix
	cmd:sed
	cmd:awk
	"

BUILD()
{
	runConfigure ./configure --without-tools \
		--enable-static
	make $jobArgs
}

INSTALL()
{
	make install

	prepareInstalledDevelLibs libqrencode
	fixPkgconfig
	
	# devel package
	packageEntries devel \
		$developDir
}

# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	qrencode${secondaryArchSuffix}_devel = $portVersion compat >= 3
	devel:libqrencode$secondaryArchSuffix = 3.3.0 compat >= 3
	"
REQUIRES_devel="
	qrencode$secondaryArchSuffix == $portVersion
	"
